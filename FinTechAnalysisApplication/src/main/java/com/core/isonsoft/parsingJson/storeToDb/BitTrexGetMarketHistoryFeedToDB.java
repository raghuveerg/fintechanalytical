/*
 * 
 */
package com.core.isonsoft.parsingJson.storeToDb;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.core.isonsoft.beans.StagBitTrexGetMarketHistoryBean;
import com.core.isonsoft.main.MaillingService;
import com.core.isonsoft.xmlParsing.HibernateUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author raghu
 *
 */
public class BitTrexGetMarketHistoryFeedToDB extends TimerTask {

	/** LOGGER */
	public final static Logger LOG = LoggerFactory
			.getLogger(BitTrexGetMarketHistoryFeedToDB.class);

	/** The Constant statusFlag. */
	public static Boolean statusFlag = Boolean.FALSE;
	/** The Constant message. */
	private static String message = null;

	/** The Constant codeDataSourceId. */
	public final static String codeDataSourceId = "34";
	/** The Constant downloadStatus. */
	public static String downloadStatus = "success";

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws JsonParseException,
			IOException {

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new BitTrexGetMarketHistoryFeedToDB(), 0,
				60 * 60 * 1000);
		/*
		 * ObjectMapper mapper = new ObjectMapper(); JsonNode rootNode =
		 * mapper.createObjectNode(); Session
		 * session=HibernateUtil.getSessionFactory().openSession();
		 * 
		 * convertJsonDataToDb(rootNode,mapper,
		 * "C:\\Users\\raghu\\Desktop\\faliure\\34.json"
		 * ,System.currentTimeMillis(),"34",session);
		 */
	}

	public static void convertJsonDataToDb(String path, long systemTime,
			Session session) {
		try {

			JsonNode rootNode = new ObjectMapper().readTree(new File(path));

			for (JsonNode jsonNode : rootNode.path("result")) {
				StagBitTrexGetMarketHistoryBean stagBitTrexGetMarketHistoryBean = new StagBitTrexGetMarketHistoryBean();
				stagBitTrexGetMarketHistoryBean.setTimeStamp(jsonNode.path(
						"TimeStamp").asText());
				stagBitTrexGetMarketHistoryBean.setIdFeed(Long
						.parseLong(jsonNode.path("Id").asText()));
				stagBitTrexGetMarketHistoryBean.setQuatity(Double
						.parseDouble(jsonNode.path("Quantity").asText()));
				stagBitTrexGetMarketHistoryBean.setPrice(Double
						.parseDouble(jsonNode.path("Price").asText()));
				stagBitTrexGetMarketHistoryBean.setTotal(Double
						.parseDouble(jsonNode.path("Total").asText()));
				stagBitTrexGetMarketHistoryBean.setFillType(jsonNode.path(
						"FillType").asText());
				stagBitTrexGetMarketHistoryBean.setOrderType(jsonNode.path(
						"OrderType").asText());
				session.save(stagBitTrexGetMarketHistoryBean);
			}
			session.getTransaction().commit();

		} catch (NullPointerException ex) {
			statusFlag = Boolean.TRUE;
			message = "The server is busy unable to create JSON File, Please check the URL for ID "
					+ codeDataSourceId;
		} catch (Exception e) {
			LOG.error("cannot convert the URL to json file {}{}", e,
					codeDataSourceId);

			statusFlag = Boolean.TRUE;
			message = InsertToStatsDownTable.convertTheExceptionToString(e);
		} finally {

			if (statusFlag) {
				downloadStatus = "failure";
				MaillingService.sendMail(codeDataSourceId, message);
			}
			LOG.info("before the insertionsss");
			if (!session.getTransaction().isActive()) {
				session.beginTransaction();
			}
			InsertToStatsDownTable.insertingDataToTab(session, message,
					codeDataSourceId, systemTime, downloadStatus);

			session.close();
		}
	}

	@Override
	public void run() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		// get the url from the codedatasource based on the ID
		String url = InsertToStatsDownTable.getUrlFromCodeDataTab(session,
				codeDataSourceId);
		try {
			/**
			 * gets the url and convert the feed data to Json file in system
			 * Specified File by ConvertUrlToJsonFile.readJsonFromUrl(url, id)
			 * 
			 * below method is used to take json file from specified path and
			 * insert into DB.
			 * 
			 */
			convertJsonDataToDb(
					ConvertUrlToJsonFile.readJsonFromUrl(url, codeDataSourceId),
					System.currentTimeMillis(), session);
		} catch (IOException e) {

			LOG.error("cannot convert the URL to json file {},{}",
					codeDataSourceId, e);
		} finally {
			session.close();
		}

	}
}
