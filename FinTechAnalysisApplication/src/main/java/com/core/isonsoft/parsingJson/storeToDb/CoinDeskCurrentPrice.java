/*
 * 
 */
package com.core.isonsoft.parsingJson.storeToDb;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.core.isonsoft.beans.CoinDeskCurrentPriceMainBean;
import com.core.isonsoft.beans.CoinDeskCurrentPriceTableDataBean;
import com.core.isonsoft.main.MaillingService;
import com.core.isonsoft.xmlParsing.HibernateUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author raghu
 *
 */

public class CoinDeskCurrentPrice extends TimerTask {
	/** LOGGER */
	public final static Logger LOG = LoggerFactory
			.getLogger(CoinDeskCurrentPrice.class);

	/** The Constant statusFlag. */
	public static Boolean statusFlag = Boolean.FALSE;
	/** The Constant message. */
	private static String message = null;

	/** The Constant id. */
	public final static String codeDataSourceId = "10";
	/** The Constant downloadStatus. */
	public static String downloadStatus = "success";

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws JsonParseException,
			IOException {

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new CoinDeskCurrentPrice(), 0, 60 * 60 * 1000);
	}

	/**
	 * 
	 * @param rootNode
	 * @param mapper
	 * @param path
	 * @param currentTimeMillis
	 * @param Id
	 * @param session
	 */
	public static void convertJsonDataToDb(String path, long currentTimeMillis,
			Session session) {
		try {

			CoinDeskCurrentPriceMainBean coinDeskCurrentPriceMainBean = new ObjectMapper()
					.readValue(new File(path),
							CoinDeskCurrentPriceMainBean.class);
			CoinDeskCurrentPriceTableDataBean tableData = new CoinDeskCurrentPriceTableDataBean();
			tableData.setEurCode(coinDeskCurrentPriceMainBean.getBpi().getEUR()
					.getCode());
			tableData.setDisclaimer(coinDeskCurrentPriceMainBean
					.getDisclaimer());
			tableData.setEurDescription(coinDeskCurrentPriceMainBean.getBpi()
					.getEUR().getDescription());
			tableData.setEurRate(coinDeskCurrentPriceMainBean.getBpi().getEUR()
					.getRate());
			tableData.setEurRate_float(coinDeskCurrentPriceMainBean.getBpi()
					.getEUR().getRate_float());
			tableData.setEurSymbol(coinDeskCurrentPriceMainBean.getBpi()
					.getEUR().getSymbol());
			tableData.setGbpCode(coinDeskCurrentPriceMainBean.getBpi().getGBP()
					.getCode());
			tableData.setGbpDescription(coinDeskCurrentPriceMainBean.getBpi()
					.getGBP().getDescription());
			tableData.setGbpRate(coinDeskCurrentPriceMainBean.getBpi().getGBP()
					.getRate());
			tableData.setGbpRate_float(coinDeskCurrentPriceMainBean.getBpi()
					.getGBP().getRate_float());
			tableData.setGbpSymbol(coinDeskCurrentPriceMainBean.getBpi()
					.getGBP().getSymbol());
			tableData.setUsdCode(coinDeskCurrentPriceMainBean.getBpi().getUSD()
					.getCode());
			tableData.setUsdDescription(coinDeskCurrentPriceMainBean.getBpi()
					.getUSD().getDescription());
			tableData.setUsdRate(coinDeskCurrentPriceMainBean.getBpi().getUSD()
					.getRate());
			tableData.setUsdRate_float(coinDeskCurrentPriceMainBean.getBpi()
					.getUSD().getRate_float());
			tableData.setUsdSymbol(coinDeskCurrentPriceMainBean.getBpi()
					.getUSD().getSymbol());
			tableData.setUpdated(coinDeskCurrentPriceMainBean.getTime()
					.getUpdated());
			tableData.setUpdatedISO(coinDeskCurrentPriceMainBean.getTime()
					.getUpdatedISO());
			tableData.setUpdateduk(coinDeskCurrentPriceMainBean.getTime()
					.getUpdateduk());
			session.save(tableData);

			session.getTransaction().commit();

		} catch (NullPointerException ex) {
			statusFlag = Boolean.TRUE;
			message = "The server is busy unable to create JSON File, Please check the URL for ID "
					+ codeDataSourceId;
		} catch (Exception e) {
			LOG.error("cannot convert the URL to json file {}{}", e,
					codeDataSourceId);

			statusFlag = Boolean.TRUE;
			message = InsertToStatsDownTable.convertTheExceptionToString(e);
		} finally {

			if (statusFlag) {
				downloadStatus = "failure";
				MaillingService.sendMail(codeDataSourceId, message);
			}
			LOG.info("before the insertionsss");
			if (!session.getTransaction().isActive()) {
				session.beginTransaction();
			}
			InsertToStatsDownTable.insertingDataToTab(session, message,
					codeDataSourceId, currentTimeMillis, downloadStatus);

			session.close();
		}

	}

	/**
	 * 
	 */
	@Override
	public void run() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		// get the url from the codedatasource based on the ID
		String url = InsertToStatsDownTable.getUrlFromCodeDataTab(session,
				codeDataSourceId);
		LOG.info(url);
		try {
			/**
			 * gets the url and convert the feed data to Json file in system
			 * Specified File by ConvertUrlToJsonFile.readJsonFromUrl(url, id)
			 * 
			 * below method is used to take json file from specified path and
			 * insert into DB.
			 * 
			 */
			convertJsonDataToDb(
					ConvertUrlToJsonFile.readJsonFromUrl(url, codeDataSourceId),
					System.currentTimeMillis(), session);
		} catch (IOException e) {
			LOG.error("cannot convert the URL to json file {}{}",
					e.fillInStackTrace(), codeDataSourceId);
		} finally {
			session.close();
		}

	}

}
