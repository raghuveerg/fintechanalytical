/*
 * 
 */
package com.core.isonsoft.parsingJson.storeToDb;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Timer;
import java.util.TimerTask;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.core.isonsoft.beans.StagCoinMarketCapTickerBean;
import com.core.isonsoft.main.MaillingService;
import com.core.isonsoft.xmlParsing.HibernateUtil;

/**
 * 
 * @author raghu
 *
 */
public class CoinMarketCapTickerBeanFeedToDB extends TimerTask {

	/** LOGGER */
	public final static Logger LOG = LoggerFactory
			.getLogger(CoinMarketCapTickerBeanFeedToDB.class);

	/** The Constant statusFlag. */
	public static Boolean statusFlag = Boolean.FALSE;
	/** The Constant message. */
	private static String message = null;

	/** The Constant id. */
	public final static String codeDataSourceId = "50";
	/** The Constant downloadStatus. */
	public static String downloadStatus = "success";

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws IOException {
		/*
		 * ObjectMapper mapper = new ObjectMapper(); JsonNode rootNode =
		 * mapper.createObjectNode(); Session
		 * session=HibernateUtil.getSessionFactory().openSession();
		 * 
		 * convertJsonDataToDb(rootNode,mapper,
		 * "C:\\Users\\raghu\\Desktop\\faliure\\50.json"
		 * ,System.currentTimeMillis(),"50",session);
		 */

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new CoinMarketCapTickerBeanFeedToDB(), 0,
				60 * 60 * 1000);
	}

	public static void convertJsonDataToDb(String path, long currentTimeMillis,
			Session session) {
		try {

			JsonNode rootNode = new ObjectMapper().readTree(new File(path));

			for (JsonNode jsonNode : rootNode) {
				StagCoinMarketCapTickerBean stagCoinMarketCapTickerBean = new StagCoinMarketCapTickerBean();
				if (jsonNode.get("available_supply").asText() != null
						&& !jsonNode.get("available_supply").asText()
								.equals("null")
						&& !(jsonNode.get("available_supply").asText()
								.isEmpty()))

					stagCoinMarketCapTickerBean.setAvailable_supply(Double
							.parseDouble(jsonNode.get("available_supply")
									.asText()));
				if (jsonNode.get("24h_volume_usd").asText() != null
						&& !jsonNode.get("24h_volume_usd").asText()
								.equals("null")
						&& !(jsonNode.get("24h_volume_usd").asText().isEmpty()))

					stagCoinMarketCapTickerBean.setH_volume_usd(Double
							.parseDouble(jsonNode.get("24h_volume_usd")
									.asText()));

				stagCoinMarketCapTickerBean.setId(jsonNode.get("id").asText());
				if (jsonNode.get("last_updated").asText() != null
						&& !jsonNode.get("last_updated").asText()
								.equals("null")
						&& !(jsonNode.get("last_updated").asText().isEmpty()))

					stagCoinMarketCapTickerBean.setLast_updated(new Timestamp(
							Long.parseLong((jsonNode.get("last_updated")
									.asText())) * 1000L));
				if (jsonNode.get("market_cap_usd").asText() != null
						&& !jsonNode.get("market_cap_usd").asText()
								.equals("null")
						&& !(jsonNode.get("market_cap_usd").asText().isEmpty()))

					stagCoinMarketCapTickerBean.setMarket_cap_usd(Double
							.parseDouble(jsonNode.get("market_cap_usd")
									.asText()));
				stagCoinMarketCapTickerBean.setName(jsonNode.get("name")
						.asText());

				if (jsonNode.get("percent_change_1h").asText() != null
						&& !jsonNode.get("percent_change_1h").asText()
								.equals("null")
						&& !(jsonNode.get("percent_change_1h").asText()
								.isEmpty()))
					stagCoinMarketCapTickerBean.setPercent_change_1h(Double
							.parseDouble(jsonNode.get("percent_change_1h")
									.asText()));

				if (jsonNode.get("percent_change_24h").asText() != null
						&& !jsonNode.get("percent_change_24h").asText()
								.equals("null")
						&& !(jsonNode.get("percent_change_24h").asText()
								.isEmpty()))

					stagCoinMarketCapTickerBean.setPercent_change_24h(Double
							.parseDouble(jsonNode.get("percent_change_24h")
									.asText()));
				if (jsonNode.get("percent_change_7d").asText() != null
						&& !jsonNode.get("percent_change_7d").asText()
								.equals("null")
						&& !(jsonNode.get("percent_change_7d").asText()
								.isEmpty()))

					stagCoinMarketCapTickerBean.setPercent_change_7d(Double
							.parseDouble(jsonNode.get("percent_change_7d")
									.asText()));
				if (jsonNode.get("price_btc").asText() != null
						&& !jsonNode.get("price_btc").asText().equals("null")
						&& !(jsonNode.get("price_btc").asText().isEmpty()))

					stagCoinMarketCapTickerBean.setPrice_btc(Double
							.parseDouble(jsonNode.get("price_btc").asText()));
				if (jsonNode.get("price_usd").asText() != null
						&& !jsonNode.get("price_usd").asText().equals("null")
						&& !(jsonNode.get("price_usd").asText().isEmpty()))

					stagCoinMarketCapTickerBean.setPrice_usd(Double
							.parseDouble(jsonNode.get("price_usd").asText()));
				if (jsonNode.get("rank").asText() != null
						&& !jsonNode.get("rank").asText().equals("null")
						&& !(jsonNode.get("rank").asText().isEmpty()))

					stagCoinMarketCapTickerBean.setRank(Long.parseLong(jsonNode
							.get("rank").asText()));
				stagCoinMarketCapTickerBean.setSymbol(jsonNode.get("symbol")
						.asText());
				if (jsonNode.get("total_supply").asText() != null
						&& !jsonNode.get("total_supply").asText()
								.equals("null")
						&& !(jsonNode.get("total_supply").asText().isEmpty()))

					stagCoinMarketCapTickerBean
							.setTotal_supply(Double.parseDouble(jsonNode.get(
									"total_supply").asText()));

				session.save(stagCoinMarketCapTickerBean);

			}

			session.getTransaction().commit();

		} catch (NullPointerException ex) {
			statusFlag = Boolean.TRUE;
			message = "The server is busy unable to create JSON File, Please check the URL for ID "
					+ codeDataSourceId;
		} catch (Exception e) {
			LOG.error("cannot convert the URL to json file {}{}", e,
					codeDataSourceId);

			statusFlag = Boolean.TRUE;
			message = InsertToStatsDownTable.convertTheExceptionToString(e);
		} finally {

			if (statusFlag) {
				downloadStatus = "failure";
				MaillingService.sendMail(codeDataSourceId, message);
			}
			LOG.info("before the insertionsss");
			if (!session.getTransaction().isActive()) {
				session.beginTransaction();
			}
			InsertToStatsDownTable.insertingDataToTab(session, message,
					codeDataSourceId, currentTimeMillis, downloadStatus);

			session.close();
		}

	}

	@Override
	public void run() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		// get the url from the codedatasource based on the ID
		String url = InsertToStatsDownTable.getUrlFromCodeDataTab(session,
				codeDataSourceId);
		try {
			/**
			 * gets the url and convert the feed data to Json file in system
			 * Specified File by ConvertUrlToJsonFile.readJsonFromUrl(url, id)
			 * 
			 * below method is used to take json file from specified path and
			 * insert into DB.
			 * 
			 */
			convertJsonDataToDb(
					ConvertUrlToJsonFile.readJsonFromUrl(url, codeDataSourceId),
					System.currentTimeMillis(), session);
		} catch (IOException e) {

			LOG.error("cannot convert the URL to json file {},{}",
					codeDataSourceId, e);
		} finally {
			session.close();
		}

	}

}
