/*
 * 
 */
package com.core.isonsoft.parsingJson.storeToDb;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.core.isonsoft.beans.StagBtceTickerMainBean;
import com.core.isonsoft.main.MaillingService;
import com.core.isonsoft.xmlParsing.HibernateUtil;

public class BtceTickerFeedToDb extends TimerTask {
	/** LOGGER */
	public final static Logger LOG = LoggerFactory
			.getLogger(BtceTickerFeedToDb.class);

	/** The Constant statusFlag. */
	public static Boolean statusFlag = Boolean.FALSE;
	/** The Constant message. */
	private static String message = null;

	/** The Constant id. */
	public final static String codeDataSourceId = "22";
	/** The Constant downloadStatus. */
	public static String downloadStatus = "success";

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws IOException {

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new BtceTickerFeedToDb(), 0, 60 * 60 * 1000);
		/*
		 * ObjectMapper mapper = new ObjectMapper(); JsonNode rootNode =
		 * mapper.createObjectNode(); Session
		 * session=HibernateUtil.getSessionFactory().openSession();
		 * 
		 * convertJsonDataToDb(rootNode,mapper,
		 * "C:\\Users\\raghu\\Desktop\\faliure\\22.json"
		 * ,System.currentTimeMillis(),"22",session);
		 */
	}

	public static void convertJsonDataToDb(String path, long systemTime,
			Session session) {

		try {

			JsonNode rootNode = new ObjectMapper().readTree(new File(path));
			Iterator<String> fieldName = rootNode.getFieldNames();
			while (fieldName.hasNext()) {
				StagBtceTickerMainBean stagBtceTickerMainBean = new StagBtceTickerMainBean();
				String fieldNames = fieldName.next();
				stagBtceTickerMainBean.setFieldName(fieldNames);
				if (rootNode.get(fieldNames) != null) {

					stagBtceTickerMainBean.setHigh(Double.parseDouble(rootNode
							.get(fieldNames).path("high").asText()));
					stagBtceTickerMainBean.setLow(Double.parseDouble(rootNode
							.get(fieldNames).path("low").asText()));
					stagBtceTickerMainBean.setAvg(Double.parseDouble(rootNode
							.get(fieldNames).path("avg").asText()));
					stagBtceTickerMainBean.setVol(Double.parseDouble(rootNode
							.get(fieldNames).path("vol").asText()));
					stagBtceTickerMainBean.setVol_cur(Double
							.parseDouble(rootNode.get(fieldNames)
									.path("vol_cur").asText()));
					stagBtceTickerMainBean.setLast(Double.parseDouble(rootNode
							.get(fieldNames).path("last").asText()));
					stagBtceTickerMainBean.setBuy(Double.parseDouble(rootNode
							.get(fieldNames).path("buy").asText()));
					stagBtceTickerMainBean.setSell(Double.parseDouble(rootNode
							.get(fieldNames).path("sell").asText()));
					stagBtceTickerMainBean.setUpdated(Long.parseLong(rootNode
							.get(fieldNames).path("updated").asText()));
				}

				session.save(stagBtceTickerMainBean);

			}
			session.getTransaction().commit();
		} catch (NullPointerException ex) {
			statusFlag = Boolean.TRUE;
			message = "The server is busy unable to create JSON File, Please check the URL for ID "
					+ codeDataSourceId;
		} catch (Exception e) {
			LOG.error("cannot convert the URL to json file {}{}", e,
					codeDataSourceId);

			statusFlag = Boolean.TRUE;
			message = InsertToStatsDownTable.convertTheExceptionToString(e);
		} finally {

			if (statusFlag) {
				downloadStatus = "failure";
				MaillingService.sendMail(codeDataSourceId, message);
			}
			LOG.info("before the insertionsss");
			if (!session.beginTransaction().isActive()) {
				session.beginTransaction();
			}
			InsertToStatsDownTable.insertingDataToTab(session, message,
					codeDataSourceId, systemTime, downloadStatus);

			session.close();
		}

	}

	@Override
	public void run() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		// get the url from the codedatasource based on the ID
		String url = InsertToStatsDownTable.getUrlFromCodeDataTab(session,
				codeDataSourceId);
		try {
			/**
			 * gets the url and convert the feed data to Json file in system
			 * Specified File by ConvertUrlToJsonFile.readJsonFromUrl(url, id)
			 * 
			 * below method is used to take json file from specified path and
			 * insert into DB.
			 * 
			 */
			convertJsonDataToDb(
					ConvertUrlToJsonFile.readJsonFromUrl(url, codeDataSourceId),
					System.currentTimeMillis(), session);
		} catch (IOException e) {

			LOG.error("cannot convert the URL to json file {},{}",
					codeDataSourceId, e);
		} finally {
			session.close();
		}

	}

}
