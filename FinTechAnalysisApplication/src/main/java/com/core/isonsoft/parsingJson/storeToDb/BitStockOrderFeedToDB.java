/*
 * 
 */
package com.core.isonsoft.parsingJson.storeToDb;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.core.isonsoft.beans.StagBitStockOrderBean;
import com.core.isonsoft.main.MaillingService;
import com.core.isonsoft.xmlParsing.HibernateUtil;

/**
 * 
 * @author raghu
 *
 */
public class BitStockOrderFeedToDB extends TimerTask {

	/** LOGGER */
	public final static Logger LOG = LoggerFactory
			.getLogger(BitStockOrderFeedToDB.class);

	/** The Constant statusFlag. */
	public static Boolean statusFlag = Boolean.FALSE;
	/** The Constant message. */
	private static String message = null;

	/** The Constant id. */
	public static String codeDataSourceId = "48";
	/** The Constant downloadStatus. */
	public static String downloadStatus = "success";

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws JsonParseException,
			IOException {
		/*
		 * ObjectMapper mapper = new ObjectMapper(); JsonNode rootNode =
		 * mapper.createObjectNode(); Session
		 * session=HibernateUtil.getSessionFactory().openSession();
		 * convertJsonDataToDb
		 * (rootNode,mapper,"C:\\Users\\raghu\\Desktop\\faliure\\48.json"
		 * ,System.currentTimeMillis(),"49",session);
		 */

		new Timer().scheduleAtFixedRate(new BitStockOrderFeedToDB(), 0,
				60 * 60 * 1000);
	}

	/**
	 * Json converter.
	 *
	 * @param rootNode
	 *            the root node
	 * @param mapper
	 *            the mapper
	 * @param path
	 *            the path
	 * @param startTime
	 */
	public static void convertJsonDataToDb(String path, long startTime,
			Session session) {

		try {

			JsonNode rootNode = new ObjectMapper().readTree(new File(path));
			if (rootNode.path("bids") != null) {

				for (JsonNode jsonNode : rootNode.path("bids")) {
					StagBitStockOrderBean stagBitStockOrderBean = new StagBitStockOrderBean();
					stagBitStockOrderBean.setAsks("0");
					stagBitStockOrderBean.setBids("1");
					stagBitStockOrderBean.setPrice(Double.parseDouble(jsonNode
							.get(0).asText()));
					stagBitStockOrderBean.setVolume(Double.parseDouble(jsonNode
							.get(1).asText()));
					stagBitStockOrderBean.setValue(Double.parseDouble(jsonNode
							.get(2).asText()));
					session.save(stagBitStockOrderBean);
				}
			}

			if (rootNode.path("asks") != null) {

				for (JsonNode jsonNode : rootNode.path("asks")) {
					StagBitStockOrderBean stagBitStockOrderBean = new StagBitStockOrderBean();
					stagBitStockOrderBean.setAsks("0");
					stagBitStockOrderBean.setBids("1");
					stagBitStockOrderBean.setPrice(Double.parseDouble(jsonNode
							.get(0).asText()));
					stagBitStockOrderBean.setVolume(Double.parseDouble(jsonNode
							.get(1).asText()));
					stagBitStockOrderBean.setValue(Double.parseDouble(jsonNode
							.get(2).asText()));
					session.save(stagBitStockOrderBean);
				}
			}
			session.getTransaction().commit();

		} catch (NullPointerException ex) {
			statusFlag = Boolean.TRUE;
			message = "The server is busy unable to create JSON File, Please check the URL for ID "
					+ codeDataSourceId;
		} catch (Exception e) {
			e.printStackTrace();
			statusFlag = Boolean.TRUE;
			message = InsertToStatsDownTable.convertTheExceptionToString(e);
		} finally {

			if (statusFlag) {
				downloadStatus = "failure";
				MaillingService.sendMail(codeDataSourceId, message);
			}
			LOG.info("before the insertionsss");
			if (!session.getTransaction().isActive()) {
				session.beginTransaction();
			}
			InsertToStatsDownTable.insertingDataToTab(session, message,
					codeDataSourceId, startTime, downloadStatus);

			session.close();
		}

	}

	@Override
	public void run() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		// get the url from the codedatasource based on the ID
		String url = InsertToStatsDownTable.getUrlFromCodeDataTab(session,
				codeDataSourceId);
		try {
			/**
			 * gets the url and convert the feed data to Json file in system
			 * Specified File by ConvertUrlToJsonFile.readJsonFromUrl(url, id)
			 * 
			 * below method is used to take json file from specified path and
			 * insert into DB.
			 * 
			 */
			convertJsonDataToDb(
					ConvertUrlToJsonFile.readJsonFromUrl(url, codeDataSourceId),
					System.currentTimeMillis(), session);
		} catch (IOException e) {

			LOG.error("cannot convert the URL to json file {},{}",
					codeDataSourceId, e);
		} finally {
			session.close();
		}
	}

}
