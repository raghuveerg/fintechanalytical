/*
 * 
 */
package com.core.isonsoft.parsingJson.storeToDb;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.core.isonsoft.beans.StagCcexGetMarketsBean;
import com.core.isonsoft.main.EpochDateConverter;
import com.core.isonsoft.main.MaillingService;
import com.core.isonsoft.xmlParsing.HibernateUtil;

/**
 * 
 * @author raghu
 *
 */
public class CcexGetMarketSFeedToDB extends TimerTask {
	/** LOGGER */
	public final static Logger LOG = LoggerFactory
			.getLogger(CcexGetMarketSFeedToDB.class);

	/** The Constant statusFlag. */
	public static Boolean statusFlag = Boolean.FALSE;
	/** The Constant message. */
	private static String message = null;

	/** The Constant id. */
	public final static String codeDataSourceId = "43";
	/** The Constant downloadStatus. */
	public static String downloadStatus = "success";

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws JsonParseException,
			IOException {
		/*
		 * ObjectMapper mapper = new ObjectMapper(); JsonNode rootNode =
		 * mapper.createObjectNode(); Session session=
		 * HibernateUtil.getSessionFactory().openSession();
		 * convertJsonDataToDb(rootNode
		 * ,mapper,"C:\\Users\\raghu\\Desktop\\faliure\\43.json"
		 * ,System.currentTimeMillis(),"43",session);
		 */
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new CcexGetMarketSFeedToDB(), 0,
				60 * 60 * 1000);
	}

	/**
	 * 
	 * @param rootNode
	 * @param mapper
	 * @param path
	 * @param systemTime
	 * @param Id
	 * @param session
	 */
	public static void convertJsonDataToDb(String path, long systemTime,
			Session session) {
		try {

			JsonNode rootNode = new ObjectMapper().readTree(new File(path));
			Iterator<String> fieldNames = rootNode.getFieldNames();

			while (fieldNames.hasNext()) {

				String fieldName = fieldNames.next();

				if (fieldName.equals("result")) {
					for (JsonNode jsonNode : rootNode.get(fieldName)) {
						StagCcexGetMarketsBean stagCcexGetMarketsBean = new StagCcexGetMarketsBean();

						stagCcexGetMarketsBean.setMarketCurrency(jsonNode.get(
								"MarketCurrency").asText());
						stagCcexGetMarketsBean.setBaseCurrency(jsonNode.get(
								"BaseCurrency").asText());
						stagCcexGetMarketsBean.setMarketCurrencyLong(jsonNode
								.get("MarketCurrencyLong").asText());
						stagCcexGetMarketsBean.setMinTradeSize(Double
								.parseDouble(jsonNode.get("MinTradeSize")
										.asText()));
						stagCcexGetMarketsBean.setMarketName(jsonNode.get(
								"MarketName").asText());
						stagCcexGetMarketsBean.setActive(Boolean
								.getBoolean(jsonNode.get("IsActive").asText()));

						stagCcexGetMarketsBean.setCreated(new Timestamp(
								EpochDateConverter.convertingSimpleDate(
										jsonNode.get("Created").asText())
										.getTime()));

						session.save(stagCcexGetMarketsBean);

					}

				}
			}

			session.getTransaction().commit();

		} catch (NullPointerException ex) {
			statusFlag = Boolean.TRUE;
			message = "The server is busy unable to create JSON File, Please check the URL for ID "
					+ codeDataSourceId;
		} catch (Exception e) {
			LOG.error("cannot convert the URL to json file {}{}", e,
					codeDataSourceId);

			statusFlag = Boolean.TRUE;
			message = InsertToStatsDownTable.convertTheExceptionToString(e);
		} finally {

			if (statusFlag) {
				downloadStatus = "failure";
				MaillingService.sendMail(codeDataSourceId, message);
			}
			LOG.info("before the insertionsss");
			if (!session.getTransaction().isActive()) {
				session.beginTransaction();
			}
			InsertToStatsDownTable.insertingDataToTab(session, message,
					codeDataSourceId, systemTime, downloadStatus);

			session.close();
		}
	}

	/**
	 * 
	 */
	@Override
	public void run() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		// get the url from the codedatasource based on the ID
		String url = InsertToStatsDownTable.getUrlFromCodeDataTab(session,
				codeDataSourceId);
		try {
			/**
			 * gets the url and convert the feed data to Json file in system
			 * Specified File by ConvertUrlToJsonFile.readJsonFromUrl(url, id)
			 * 
			 * below method is used to take json file from specified path and
			 * insert into DB.
			 * 
			 */
			convertJsonDataToDb(
					ConvertUrlToJsonFile.readJsonFromUrl(url, codeDataSourceId),
					System.currentTimeMillis(), session);
		} catch (IOException e) {

			LOG.error("cannot convert the URL to json file {},{}",
					codeDataSourceId, e);
		} finally {
			session.close();
		}

	}

}
