/*
 * 
 */
package com.core.isonsoft.parsingJson.storeToDb;

import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.core.isonsoft.beans.StagBitTrexGetMarketSummariesBean;
import com.core.isonsoft.main.MaillingService;
import com.core.isonsoft.xmlParsing.HibernateUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BitTrexGetMarketSummariesFeedToDB extends TimerTask {

	/** LOGGER */
	public final static Logger LOG = LoggerFactory
			.getLogger(BitTrexGetMarketSummariesFeedToDB.class);

	/** The Constant statusFlag. */
	public static Boolean statusFlag = Boolean.FALSE;
	/** The Constant message. */
	private static String message = null;

	/** The Constant id. */
	public final static String codeDataSourceId = "32";
	/** The Constant downloadStatus. */
	public static String downloadStatus = "success";

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws JsonParseException,
			IOException {
		/*
		 * ObjectMapper mapper = new ObjectMapper(); JsonNode rootNode =
		 * mapper.createObjectNode(); Session
		 * session=HibernateUtil.getSessionFactory().openSession();
		 * 
		 * convertJsonDataToDb(rootNode,mapper,
		 * "C:\\Users\\raghu\\Desktop\\faliure\\32.json"
		 * ,System.currentTimeMillis(),"32",session);
		 */

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new BitTrexGetMarketSummariesFeedToDB(), 0,
				60 * 60 * 1000);
	}

	public static void convertJsonDataToDb(String path, long systemTime,
			Session session) {
		try {
			JsonNode rootNode = new ObjectMapper().readTree(new File(path));

			for (JsonNode jsonNode : rootNode.path("result")) {
				StagBitTrexGetMarketSummariesBean stagBitTrexGetCurrenciesBean = new StagBitTrexGetMarketSummariesBean();

				if (jsonNode.path("Ask").asText() != null
						&& !jsonNode.path("Ask").asText().isEmpty()) {
					stagBitTrexGetCurrenciesBean.setAsk(Double
							.parseDouble(jsonNode.path("Ask").asText()));
				}
				if (jsonNode.path("BaseVolume").asText() != null
						&& !jsonNode.path("BaseVolume").asText().isEmpty()) {
					stagBitTrexGetCurrenciesBean.setBaseVolume(Double
							.parseDouble(jsonNode.path("BaseVolume").asText()));

				}
				if (jsonNode.path("Bid").asText() != null
						&& !jsonNode.path("Bid").asText().isEmpty()) {
					stagBitTrexGetCurrenciesBean.setBid(Double
							.parseDouble(jsonNode.path("Bid").asText()));
				}
				stagBitTrexGetCurrenciesBean.setCreated(jsonNode
						.path("Created").asText());
				if (jsonNode.path("High").asText() != null
						&& !jsonNode.path("High").asText().isEmpty()) {
					stagBitTrexGetCurrenciesBean.setHigh(Double
							.parseDouble(jsonNode.path("High").asText()));

				}
				if (jsonNode.path("Last").asText() != null
						&& !jsonNode.path("Last").asText().isEmpty()) {
					stagBitTrexGetCurrenciesBean.setLast(Double
							.parseDouble(jsonNode.path("Last").asText()));
				}
				if (jsonNode.path("Low").asText() != null
						&& !jsonNode.path("Low").asText().isEmpty()) {
					stagBitTrexGetCurrenciesBean.setLow(Double
							.parseDouble(jsonNode.path("Low").asText()));
				}
				stagBitTrexGetCurrenciesBean.setMarketName(jsonNode.path(
						"MarketName").asText());
				if (jsonNode.path("OpenBuyOrders").asText() != null
						&& !jsonNode.path("OpenBuyOrders").asText().isEmpty()) {
					stagBitTrexGetCurrenciesBean.setOpenBuyOrders(Double
							.parseDouble(jsonNode.path("OpenBuyOrders")
									.asText()));
				}
				if (jsonNode.path("OpenSellOrders").asText() != null
						&& !jsonNode.path("OpenSellOrders").asText().isEmpty()) {
					stagBitTrexGetCurrenciesBean.setOpenSellOrders(Double
							.parseDouble(jsonNode.path("OpenSellOrders")
									.asText()));
				}
				if (jsonNode.path("PrevDay").asText() != null
						&& !jsonNode.path("PrevDay").asText().isEmpty()) {
					stagBitTrexGetCurrenciesBean.setPrevDay(Double
							.parseDouble(jsonNode.path("PrevDay").asText()));
				}
				stagBitTrexGetCurrenciesBean.setTimeStamp(jsonNode.path(
						"TimeStamp").asText());
				if (jsonNode.path("Volume").asText() != null
						&& !jsonNode.path("Volume").asText().isEmpty()) {
					stagBitTrexGetCurrenciesBean.setVoulme(Double
							.parseDouble(jsonNode.path("Volume").asText()));
				}
				session.save(stagBitTrexGetCurrenciesBean);

			}
			session.getTransaction().commit();

		} catch (NullPointerException ex) {
			statusFlag = Boolean.TRUE;
			message = "The server is busy unable to create JSON File, Please check the URL for ID "
					+ codeDataSourceId;
		} catch (Exception e) {
			LOG.error("cannot convert the URL to json file {}{}", e,
					codeDataSourceId);

			statusFlag = Boolean.TRUE;
			message = InsertToStatsDownTable.convertTheExceptionToString(e);
		} finally {

			if (statusFlag) {
				downloadStatus = "failure";
				MaillingService.sendMail(codeDataSourceId, message);
			}
			LOG.info("before the insertionsss");
			if (!session.getTransaction().isActive()) {
				session.beginTransaction();
			}
			InsertToStatsDownTable.insertingDataToTab(session, message,
					codeDataSourceId, systemTime, downloadStatus);

			session.close();
		}
	}

	@Override
	public void run() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		// get the url from the codedatasource based on the ID
		String url = InsertToStatsDownTable.getUrlFromCodeDataTab(session,
				codeDataSourceId);
		try {
			/**
			 * gets the url and convert the feed data to Json file in system
			 * Specified File by ConvertUrlToJsonFile.readJsonFromUrl(url, id)
			 * 
			 * below method is used to take json file from specified path and
			 * insert into DB.
			 * 
			 */
			convertJsonDataToDb(
					ConvertUrlToJsonFile.readJsonFromUrl(url, codeDataSourceId),
					System.currentTimeMillis(), session);
		} catch (IOException e) {

			LOG.error("cannot convert the URL to json file {},{}",
					codeDataSourceId, e);
		} finally {
			session.close();
		}

	}

}
