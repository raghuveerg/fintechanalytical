package com.core.isonsoft.beans;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "StagAllCoinTrades")
public class StagAllCoinTradesBean {

	public int id;
	public Timestamp dateFeed;
	public Timestamp dateFeed_ms;
	
	
	

	public double amount;
	public double price;
	public String type;
	public long tid;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getDateFeed() {
		return dateFeed;
	}

	

	public Timestamp getDateFeed_ms() {
		return dateFeed_ms;
	}

	

	
	public void setDateFeed(Timestamp dateFeed) {
		this.dateFeed = dateFeed;
	}

	public void setDateFeed_ms(Timestamp dateFeed_ms) {
		this.dateFeed_ms = dateFeed_ms;
	}
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getTid() {
		return tid;
	}

	public void setTid(long tid) {
		this.tid = tid;
	}

	@Override
	public String toString() {
		return "StagAllCoinTrades [id=" + id + ", dateFeed=" + dateFeed
				+ ", dateFeed_ms=" + dateFeed_ms + ", amount=" + amount
				+ ", price=" + price + ", sell=" + type + ", tid=" + tid + "]";
	}

	public StagAllCoinTradesBean(int id, Timestamp dateFeed, Timestamp dateFeed_ms,
			double amount, double price, String sell, long tid) {
		super();
		this.id = id;
		this.dateFeed = dateFeed;
		this.dateFeed_ms = dateFeed_ms;
		this.amount = amount;
		this.price = price;
		this.type = sell;
		this.tid = tid;
	}

	public StagAllCoinTradesBean() {

	}

}
