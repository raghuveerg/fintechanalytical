package com.core.isonsoft.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "StagBitTrexGetMarkets")
public class StagBitTrexGetMarketsBean {

	public int id;
	public String marketCurrency;
	public String baseCurrency;
	public String marketCurrencyLong;
	public String baseCurrencyLong;
	public double minTradeSize;
	public String marketName;
	public boolean isActive;
	public String created;
	public String notice;
	public String isSponsored;
	public String logoUrl;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarketCurrency() {
		return marketCurrency;
	}

	public void setMarketCurrency(String marketCurrency) {
		this.marketCurrency = marketCurrency;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getMarketCurrencyLong() {
		return marketCurrencyLong;
	}

	public void setMarketCurrencyLong(String marketCurrencyLong) {
		this.marketCurrencyLong = marketCurrencyLong;
	}

	public String getBaseCurrencyLong() {
		return baseCurrencyLong;
	}

	public void setBaseCurrencyLong(String baseCurrencyLong) {
		this.baseCurrencyLong = baseCurrencyLong;
	}

	public double getMinTradeSize() {
		return minTradeSize;
	}

	public void setMinTradeSize(double minTradeSize) {
		this.minTradeSize = minTradeSize;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {
		this.marketName = marketName;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String date) {
		this.created = date;
	}

	@Column(length = 1000)
	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public String getIsSponsored() {
		return isSponsored;
	}

	public void setIsSponsored(String isSponsored) {
		this.isSponsored = isSponsored;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	@Override
	public String toString() {
		return "StagBitTrexGetMarketsBean [id=" + id + ", marketCurrency="
				+ marketCurrency + ", baseCurrency=" + baseCurrency
				+ ", marketCurrencyLong=" + marketCurrencyLong
				+ ", baseCurrencyLong=" + baseCurrencyLong + ", minTradeSize="
				+ minTradeSize + ", marketName=" + marketName + ", isActive="
				+ isActive + ", created=" + created + ", notice=" + notice
				+ ", isSponsored=" + isSponsored + ", logoUrl=" + logoUrl + "]";
	}

	public StagBitTrexGetMarketsBean(int id, String marketCurrency,
			String baseCurrency, String marketCurrencyLong,
			String baseCurrencyLong, double minTradeSize, String marketName,
			boolean isActive, String created, String notice,
			String isSponsored, String logoUrl) {
		super();
		this.id = id;
		this.marketCurrency = marketCurrency;
		this.baseCurrency = baseCurrency;
		this.marketCurrencyLong = marketCurrencyLong;
		this.baseCurrencyLong = baseCurrencyLong;
		this.minTradeSize = minTradeSize;
		this.marketName = marketName;
		this.isActive = isActive;
		this.created = created;
		this.notice = notice;
		this.isSponsored = isSponsored;
		this.logoUrl = logoUrl;
	}

	public StagBitTrexGetMarketsBean() {

	}

}
