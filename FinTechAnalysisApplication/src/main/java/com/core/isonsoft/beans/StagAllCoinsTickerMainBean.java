package com.core.isonsoft.beans;

public class StagAllCoinsTickerMainBean {

	public String date;
	public StagAllCoinsTickerBean ticker;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public StagAllCoinsTickerBean getTicker() {
		return ticker;
	}

	public void setTicker(StagAllCoinsTickerBean ticker) {
		this.ticker = ticker;
	}

	public StagAllCoinsTickerMainBean(String date, StagAllCoinsTickerBean ticker) {
		super();
		this.date = date;
		this.ticker = ticker;
	}

	@Override
	public String toString() {
		return "StagAllCoinsTickerMainBean [date=" + date + ", ticker="
				+ ticker + "]";
	}

	public StagAllCoinsTickerMainBean() {

	}

}
