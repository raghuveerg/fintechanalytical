package com.core.isonsoft.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "StagBitTrexGetMarketSummaries")
public class StagBitTrexGetMarketSummariesBean {

	public int id;
	public String marketName;
	public double high;
	public double low;
	public double voulme;
	public double last;
	public double baseVolume;
	public String created;
	public String timeStamp;
	public double bid;
	public double ask;
	public double openBuyOrders;
	public double openSellOrders;
	public double prevDay;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarketName() {
		return marketName;
	}

	public void setMarketName(String marketName) {

		this.marketName = marketName;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getVoulme() {
		return voulme;
	}

	public void setVoulme(double voulme) {
		this.voulme = voulme;
	}

	public double getLast() {
		return last;
	}

	public void setLast(double last) {
		this.last = last;
	}

	public double getBaseVolume() {
		return baseVolume;
	}

	public void setBaseVolume(double baseVolume) {
		this.baseVolume = baseVolume;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public double getBid() {
		return bid;
	}

	public void setBid(double bid) {
		this.bid = bid;
	}

	public double getAsk() {
		return ask;
	}

	public void setAsk(double ask) {
		this.ask = ask;
	}

	public double getOpenBuyOrders() {
		return openBuyOrders;
	}

	public void setOpenBuyOrders(double openBuyOrders) {
		this.openBuyOrders = openBuyOrders;
	}

	public double getOpenSellOrders() {
		return openSellOrders;
	}

	public void setOpenSellOrders(double openSellOrders) {
		this.openSellOrders = openSellOrders;
	}

	public double getPrevDay() {
		return prevDay;
	}

	public void setPrevDay(double prevDay) {
		this.prevDay = prevDay;
	}

	public StagBitTrexGetMarketSummariesBean(int id, String marketName,
			double high, double low, double voulme, double last,
			double baseVolume, String created, String timeStamp, double bid,
			double ask, double openBuyOrders, double openSellOrders,
			double prevDay) {
		super();
		this.id = id;
		this.marketName = marketName;
		this.high = high;
		this.low = low;
		this.voulme = voulme;
		this.last = last;
		this.baseVolume = baseVolume;
		this.created = created;
		this.timeStamp = timeStamp;
		this.bid = bid;
		this.ask = ask;
		this.openBuyOrders = openBuyOrders;
		this.openSellOrders = openSellOrders;
		this.prevDay = prevDay;
	}

	public StagBitTrexGetMarketSummariesBean() {

	}

}
