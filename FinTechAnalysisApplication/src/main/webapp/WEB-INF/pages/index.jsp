

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<body style="width: 100%;height: 100%;">
	<table border="1">
		<tr>
			<td style="height: 20%">
				<!-- HEADER -->
				<jsp:include page="Header.jsp"/>
			</td>
		</tr>
		<tr>
			<td>
				<table style="height: 80%;width: 100%" >
					<tr>
						<td><jsp:include page="LeftNavigation.jsp"/></td>
						<td><jsp:include page="CenterBody.jsp"/></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<!-- footer -->
				<jsp:include page="footer.jsp"/>
			</td>
		</tr>
	</table>
		
		<!-- body -->
		
		
	</body>
</html>
